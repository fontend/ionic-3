import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  user: any;
  constructor(public navCtrl: NavController, public http: Http) {
      this.load();
  }

  load(){
    this.http.get('https://jsonplaceholder.typicode.com/users/1').map(res => res.json()).subscribe(data => {
      this.user = data;
      alert(`User Info : ${JSON.stringify(this.user)}`);
      });

  }
}
